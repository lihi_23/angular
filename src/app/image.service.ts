import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-74fc0.appspot.com/o/'
  public images:string[]= [];

  constructor() { 
    this.images[0] = this.path + 'biz.jpeg' + '?alt=media&token=5b4a6c65-a861-403f-aec9-25684944a0fb';
    this.images[1] = this.path + 'enter.jpeg' + '?alt=media&token=ff6f0585-5679-4e29-a333-ce9c156c7f5e';
    this.images[2] = this.path + 'politics-icon.jpeg' + '?alt=media&token=267519cd-7c70-4946-8bcf-ff54a39d74da';
    this.images[3] = this.path + 'sport.jpeg' + '?alt=media&token=be8d772c-af65-4bd9-a1d2-3262e5b35ffe';
    this.images[4] = this.path + 'tech.jpeg' + '?alt=media&token=d90d4542-d99c-468a-b4f1-f46caddc5cf5';
  }
}

import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "407ca8cfea6f7a4cc35238c152c8a5f4";
  private IMP = "units=metric";
  constructor( private http: HttpClient) { }

  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )

  }

  private handleError(res:HttpErrorResponse){
   console.log(res.error);
   return throwError(res.error || 'Server error');
  }

  private transformWeatherData(data:WeatherRaw):Weather{
    return{
      name:data.name,
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon

    }
  }
    }


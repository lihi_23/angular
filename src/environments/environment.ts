// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAFR2mOj94WFaGJOHJp1SjN0mHbjaqqBfI",
    authDomain: "hello-74fc0.firebaseapp.com",
    databaseURL: "https://hello-74fc0.firebaseio.com",
    projectId: "hello-74fc0",
    storageBucket: "hello-74fc0.appspot.com",
    messagingSenderId: "749298298733",
    appId: "1:749298298733:web:2e761024fee633f489f464"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
